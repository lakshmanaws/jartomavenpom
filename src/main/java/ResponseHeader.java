
/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

public class ResponseHeader {
	 public float status;
	 public float QTime;
	 public Params Params;

	 public float getStatus() {
		  return status;
	 }

	 public void setStatus(float status) {
		  this.status = status;
	 }

	 public float getQTime() {
		  return QTime;
	 }

	 public void setQTime(float QTime) {
		  this.QTime = QTime;
	 }

	 public Params getParams() {
		  return Params;
	 }

	 public void setParams(Params params) {
		  Params = params;
	 }
}
