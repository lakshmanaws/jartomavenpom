/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

import java.util.List;

public class Artifact {
	 public String       id;
	 public String       g;
	 public String       a;
	 public String       v;
	 public String       p;
	 public String       timestamp;
	 public List<String> ec;
	 public List<String> tags;

	 public String getId() {
		  return id;
	 }

	 public void setId(String id) {
		  this.id = id;
	 }

	 public String getG() {
		  return g;
	 }

	 public void setG(String g) {
		  this.g = g;
	 }

	 public String getA() {
		  return a;
	 }

	 public void setA(String a) {
		  this.a = a;
	 }

	 public String getV() {
		  return v;
	 }

	 public void setV(String v) {
		  this.v = v;
	 }

	 public String getP() {
		  return p;
	 }

	 public void setP(String p) {
		  this.p = p;
	 }

	 public String getTimestamp() {
		  return timestamp;
	 }

	 public void setTimestamp(String timestamp) {
		  this.timestamp = timestamp;
	 }

	 public List<String> getEc() {
		  return ec;
	 }

	 public void setEc(List<String> ec) {
		  this.ec = ec;
	 }

	 public List<String> getTags() {
		  return tags;
	 }

	 public void setTags(List<String> tags) {
		  this.tags = tags;
	 }

	 @Override public String toString() {
		  return "Docs{" + "id='" + id + '\'' + ", g='" + g + '\'' + ", a='" + a + '\'' + ", v='" + v + '\'' + ", p='" + p + '\'' + ", timestamp='" + timestamp + '\'' + ", ec=" + ec
				  + ", tags=" + tags + '}';
	 }
}