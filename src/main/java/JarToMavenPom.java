/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.*;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

public class JarToMavenPom {
	 private static final String OUTPUT = "out";

	 private static final String WORKSPACE = "root";

	 private static final String LIB_PATHS = "libPaths";

	 private static final String MAVEN_URL_JSON = "mvnUrl";

	 private static final String MISSING_JARS_GROUP = "missingGroupId";

	 private static final String PATH_TO_POM_PROPERTY_MAPPING = "pathToPomPropertyMapping";

	 private static final String MAVEN_SEARCH_ENABLED = "mvnSearch";

	 /**
	  * @param args out 			= full output file path | default = ./scraped-pom-dependencies.xml
	  *             root 		= the root where to search for libPaths | default = .
	  *             libPaths   = the relative paths (relative to root) where to search for jars | deault .
	  *             mvnUrl 		= maven url used to search dependencies | default https://search.maven.org/solrsearch
	  *             missingGroupId  = group id used for jars which aren't on maven repository | default it.hackubau
	  *             mvnSearch = true: search your jar on maven , false: write pom to use local jars | default true
	  *             pathToPomPropertyMapping = local jar path which you want to write in pom.xml as property instead | default ""
	  *             Example:
	  *             java -jar output=./output.xml workspace=/home/my-project libPaths=WebContent/WEB-INF/lib;lib
	  *             java -jar output=./output.xml workspace=/home/my-project libPaths=WebContent/WEB-INF/lib;lib pathToPomPropertyMapping="lib=${project.lib};WebContent/WEB-INF/lib=${project.weblib}"
	  *             java -jar output=./output.xml workspace=/home/my-project libPaths=WebContent/WEB-INF/lib;lib missingGroupId=it.hackubau pathToPomPropertyMapping="lib=${project.lib};WebContent/WEB-INF/lib=${project.weblib}"
	  */
	 public static void main(String[] args) {

		  String mavenUrl = "https://search.maven.org";
		  HashMap<String, String> defaultParams = new HashMap();

		  //initializing defaults:
		  defaultParams.put(OUTPUT, "." + File.separator + "scraped-pom-dependencies.xml");
		  defaultParams.put(WORKSPACE, ".");
		  defaultParams.put(LIB_PATHS, ".");
		  defaultParams.put(MAVEN_URL_JSON, mavenUrl + "/solrsearch");
		  defaultParams.put(MISSING_JARS_GROUP, "it.hackubau");
		  defaultParams.put(MAVEN_SEARCH_ENABLED, "true");
		  defaultParams.put(PATH_TO_POM_PROPERTY_MAPPING, "");

		  List<String> params = new ArrayList<>();
		  params.add(OUTPUT);
		  params.add(WORKSPACE);
		  params.add(LIB_PATHS);
		  params.add(MAVEN_SEARCH_ENABLED);
		  params.add(MAVEN_URL_JSON);
		  params.add(PATH_TO_POM_PROPERTY_MAPPING);

		  Map<String, String> inputParams = params.stream()
				  .collect(Collectors.toMap(k -> k, x -> Arrays.stream(args).filter(y -> y.startsWith(x)).findFirst().orElse(defaultParams.get(x)).replaceFirst(x + "=", "")));
		  System.out.println("\n===========Remapped Arguments===========");
		  inputParams.forEach((key, value) -> System.out.println(key + " > " + value));

		  final boolean getFromMaven = Boolean.parseBoolean(inputParams.get(MAVEN_SEARCH_ENABLED));

		  Map<String, String> pathToPropMappings = Arrays.stream(inputParams.get(PATH_TO_POM_PROPERTY_MAPPING).split(";")).map(x -> x.split("=")).filter(x -> x.length == 2)
				  .collect(Collectors.toMap(x -> x[0], x -> x[1]));
		  System.out.println("\n===========PathToPomPropertyMappings===========");
		  pathToPropMappings.forEach((key, value) -> System.out.println(key + " = " + value));

		  System.out.println("\n===========Search Paths===========");
		  Arrays.stream(inputParams.get(LIB_PATHS).split(";")).forEach(x -> System.out.println(inputParams.get(WORKSPACE) + File.separator + x));
		  System.out.println("\n\n");
		  try (BufferedWriter writer = new BufferedWriter(new FileWriter(inputParams.get(OUTPUT), false))) {

				for (String path : inputParams.get(LIB_PATHS).split(";")) {
					 String fullPath = inputParams.get(WORKSPACE) + File.separator + path;
					 System.out.println("Searching in :" + fullPath);
					 writer.append("\n");
					 writer.append("<!-- LIBRARIES MIGRATED FROM: ").append(path).append(" -->");
					 writer.append("\n");
					 File fileName = new File(fullPath);
					 File[] fileList = fileName.listFiles();
					 for (File file : Objects.requireNonNull(fileList)) {
						  if (file.getName().split(".jar").length == 1) {

								try {
									 String sha = calcSHA1(file);
									 String address = mavenUrl + "/search?q=1:" + sha;
									 String jsonAddress = "https://search.maven.org/solrsearch/select?q=1:" + sha;

									 writer.append("\n");
									 writer.append("\n");
									 writer.append("<!-- JARNAME: ").append(file.getName()).append("-->\n");
									 writer.append("<!-- SHA KEY: ").append(sha).append("-->\n");
									 writer.append("<!-- MAVEN URL: ").append(address).append("-->\n");
									 writer.append("<!-- MAVEN JSON URL: ").append(jsonAddress).append("-->\n");
									 MavenResponse mvnObj = null;
									 if (getFromMaven) {
										  Reader reader = new InputStreamReader(new URL(jsonAddress).openStream()); //Read the json output

										  //json converter
										  ObjectMapper objectMapper = new ObjectMapper();
										  mvnObj = objectMapper.readValue(reader, MavenResponse.class);
									 }

									 String baseDir = pathToPropMappings.get(path)!=null?pathToPropMappings.get(path):path;


									 String simpleName = file.getName().replace(".jar", "").replace("-", "").replace("_", "").replace(".", "");
									 String groupId = inputParams.get(MISSING_JARS_GROUP) + simpleName;
									 String artifactId = simpleName;
									 String version = "1.0";
									 String scope = "system";
									 String systemPath = baseDir + File.separator + file.getName();
									 if (getFromMaven && !mvnObj.getResponse().getDocs().isEmpty()) {
										  Artifact obj = mvnObj.getResponse().getDocs().get(0);
										  groupId = obj.getG();
										  artifactId = obj.getA();
										  version = obj.getV();
									 }

									 String pom = "<dependency>\n";
									 pom += "\t<groupId>" + groupId + "</groupId>\n";
									 pom += "\t<artifactId>" + artifactId + "</artifactId>\n";
									 pom += "\t<version>" + version + "</version>\n";
									 if (!getFromMaven || mvnObj.getResponse().getDocs().isEmpty()) {
										  pom += "\t<scope>" + scope + "</scope>\n";
										  pom += "\t<systemPath>" + systemPath + "</systemPath>\n";
									 }
									 if (groupId.equals("net.sf.json-lib") && artifactId.equals("json-lib") && version.equals("2.2.3")) {
										  pom += "\t<classifier>jdk13</classifier>\n";
									 }
									 pom += "</dependency>";
									 writer.append(pom);
									 writer.append("\n");
									 writer.append("\n");
									 //System.out.println(pom);

								} catch (Exception e) {
									 e.printStackTrace();
								}
						  }
					 }
				}
		  } catch (Exception err) {
				err.printStackTrace();
		  }

	 }

	 private static String calcSHA1(File file) throws IOException, NoSuchAlgorithmException {

		  MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
		  try (InputStream input = new FileInputStream(file)) {

				byte[] buffer = new byte[8192];
				int len = input.read(buffer);

				while (len != -1) {
					 sha1.update(buffer, 0, len);
					 len = input.read(buffer);
				}

				return new HexBinaryAdapter().marshal(sha1.digest());
		  }
	 }

}
