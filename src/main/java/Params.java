/*
 * Developer:  Marco Guassone - License: GNU GPLv3
 */

public class Params {
	 public String q;
	 public String core;
	 public String indent;
	 public String fl;
	 public String start;
	 public String sort;
	 public String rows;
	 public String wt;
	 public String version;

	 public String getQ() {
		  return q;
	 }

	 public void setQ(String q) {
		  this.q = q;
	 }

	 public String getCore() {
		  return core;
	 }

	 public void setCore(String core) {
		  this.core = core;
	 }

	 public String getIndent() {
		  return indent;
	 }

	 public void setIndent(String indent) {
		  this.indent = indent;
	 }

	 public String getFl() {
		  return fl;
	 }

	 public void setFl(String fl) {
		  this.fl = fl;
	 }

	 public String getStart() {
		  return start;
	 }

	 public void setStart(String start) {
		  this.start = start;
	 }

	 public String getSort() {
		  return sort;
	 }

	 public void setSort(String sort) {
		  this.sort = sort;
	 }

	 public String getRows() {
		  return rows;
	 }

	 public void setRows(String rows) {
		  this.rows = rows;
	 }

	 public String getWt() {
		  return wt;
	 }

	 public void setWt(String wt) {
		  this.wt = wt;
	 }

	 public String getVersion() {
		  return version;
	 }

	 public void setVersion(String version) {
		  this.version = version;
	 }
}