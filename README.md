# jarToMavenPom
####it may work under linux and windows too

Generate pom.xml starting by folders containing all your local jars and researching them using sha-1 queries on maven central repo.

###You will need internet connection in order to call maven url (https://search.maven.org/solrsearch)

All parameters are optionals:
```
-   out 	    = full output file path | default = ./scraped-pom-dependencies.xml
-   root	    = the root where to search for libPaths | default = .
-   libPaths        = the relative paths (relative to root) where to search for jars | deault .
-   mvnUrl 	    = maven url used to search dependencies | default https://search.maven.org/solrsearch
-   missingGroupId  = group id used for jars which aren't on maven repository | default it.hackubau
-   mvnSearch = true: search your jar on maven , false: write pom to use local jars | default true
-   pathToPomPropertyMapping = local jar path which you want to write in pom.xml as property instead | default ""  
```
Example:
```
java -jar output=./output.xml workspace=/home/my-project libPaths=WebContent/WEB-INF/lib;lib
java -jar output=./output.xml workspace=/home/my-project libPaths=WebContent/WEB-INF/lib;lib pathToPomPropertyMapping="lib=${project.lib};WebContent/WEB-INF/lib=${project.weblib}"
java -jar output=./output.xml workspace=/home/my-project libPaths=WebContent/WEB-INF/lib;lib missingGroupId=it.hackubau pathToPomPropertyMapping="lib=${project.lib};WebContent/WEB-INF/lib=${project.weblib}"
```